import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from datetime import timedelta
from DataPull import *
import pgeocode
import json



# start date of data
start_date = datetime(2020,1,22)

class Predictor:

    """
    At the moment, only a seven day prediction, but we can easily expand on this
    TODO: expand on this
    """

    def __init__(self):

        self.dp = DataPull()
        # print(os.getcwd())
        with open('user_data.json') as f:
            data = json.load(f)
            self.zipcode = data["zipcode"]


        nomi = pgeocode.Nominatim('us')
        df = nomi.query_postal_code(self.zipcode)
        self.county = df.loc['county_name']
        self.state = df.loc['state_name']

        del df, nomi

    # Predictor may be a misnomer
    def getCovidUpdate(self,state,county):

        """ Returns string with covid update for county since yesterday """

        mru, smru = self.dp.getCountyCasesYesterday(state,county)

        if mru == smru:
            return f"There have been no new cases since yesterday. There are currently {mru} cases in {county} county, {state}."
        else:
            if mru > smru:
                return f"Cases in {county} county, {state} have increased by {mru-smru} since the last update. There are currently {mru} cases."
            elif mru < smru:
                return f"Cases in {county} county, {state} have decreased by {smru-mru} since the last update. There are currently {mru} cases."
            
    def getCurrentCases(self,state=None,county=None,country=None):

        return self.dp.getCurrentCases(state=state,county=county,country=country)

    def getPrediction(self,state=None,county=None,country=None,days=7):

        next_week = self.forecastUS_linreg(state=state,county=county)[-1]

        now = self.dp.getCurrentCases(state=state,county=county,country=country)

        if next_week == now:
            return "There will not be an increase or decrease in cases in the next seven days."
        elif next_week > now:
            return f"There will be a {round(100*(next_week-now)/now)} percent increase in cases in the next seven days"
        elif next_week < now:
            return f"There will be a {round(100*(now-next_week)/now)} percent increase in cases in the next seven days"

    def forecastUS_prophet(self,state=None,county=None):

        """ Seven day prediction using Prophet """

        from fbprophet import Prophet
        from suppressor import suppress_stdout_stderr

        # suppress logging errors
        import logging
        logger = logging.getLogger('numexpr.utils')
        logger.propagate = False
        logger = logging.getLogger('fbprophet')
        logger.propagate = False

        # get case timeline
        cases = self.dp.getCaseTimelineUS(state=state,county=county)
        
        # get datetime 
        dates = []
        date = start_date
        for i in range(len(cases)):
            dates.append(date)
            date = date + timedelta(days=1)

        # formats dataframe for Prophet
        df = pd.DataFrame(list(zip(dates, cases)), columns=['ds', 'y'])

        # suppresses a bunch of annoying console messages from Prophet
        with suppress_stdout_stderr():

            p = Prophet(interval_width=0.95)
            p.fit(df)

            # forecast 7 days in advance, predict future
            future = p.make_future_dataframe(7)
            forecast = p.predict(future)

        # magic line of code to get next 7 day case predictions as a list
        forecast = [round(i[0]) for i in forecast[['yhat']].tail(88).values.tolist()]

        return forecast

    def forecastUS_linreg(self,state=None,county=None):

        from sklearn.model_selection import train_test_split 
        from sklearn.linear_model import LinearRegression
        
        """ Seven day prediction using Linear Regression """
        
        # get case timeline
        cases = self.dp.getCaseTimelineUS(state=state,county=county)
        
        # create and format cases from last 7 days
        days = np.array([i for i in range(len(cases))][-7:]).reshape(-1,1)
        cases_last_week = np.array(cases[-7:]).reshape(-1,1)

        # split training and testing data
        X_train, X_test, y_train, y_test = train_test_split(days, cases_last_week, test_size=0.2, random_state=0)

        # fit linear regression model
        lr = LinearRegression()
        lr.fit(X_train,y_train)

        # create and format array for next seven days
        future = np.array([i for i in range(len(cases) + 7)]).reshape(-1,1)
        
        # predict future
        forecast = lr.predict(future)

        # format forecast
        forecast = forecast.reshape(1,-1)[0].tolist()
        forecast = [round(i) for i in forecast]

        return forecast

    def forecastUS_polyfit(self,state=None,county=None):

        """ Seven day prediction using Least-Square Polynomial Fitting """

        # get case timeline
        cases = self.dp.getCaseTimelineUS(state=state,county=county)
        
        # create and format days and cases
        days = np.array([i for i in range(len(cases))])
        cases = np.array(cases)

        # fit a third degree polynomial curve
        polyfit = np.polynomial.polynomial.polyfit(days,cases,3)
        
        # define formula as function
        fml = lambda x:polyfit[0]+polyfit[1]*x+polyfit[2]*x**2+polyfit[3]*x**3

        case_plus_7 = [i for i in range(len(cases) + 7)]

        # get forecast for next 7 days
        forecast = []
        # for i in range(len(cases), len(cases) + 7):
        #     forecast.append(int(fml(i)))

        for i in range(0, len(cases) + 7):
            forecast.append(int(fml(i)))

        return forecast


if __name__ == "__main__":
    pred = Predictor()
    # print("Seven day prediction using:\n")
    # print("Prophet:".ljust(30, ' '), end=" ")
    # print(pred.forecastUS_prophet(state="Indiana"))
    # print("Linear regression:".ljust(30, ' '), end=" ")
    # print(pred.forecastUS_linreg(state="Indiana"))
    # print("Polynomial fitting:".ljust(30, ' '), end=" ")
    # print(pred.forecastUS_polyfit(state="Indiana"))

    p = Predictor()
    dp = DataPull()

    state = "Indiana"
    county = "Monroe"
    print("\n\n", county, state, "\n\n")
    print(dp.getCaseTimelineUS(state=state,county=county))
    print(pred.forecastUS_polyfit(state=state,county=county))
    print(pred.forecastUS_linreg(state=state,county=county))
    print(pred.forecastUS_prophet(state=state,county=county))

    state = "Indiana"
    county = "Marion"
    print("\n\n", county, state, "\n\n")
    print(dp.getCaseTimelineUS(state=state,county=county))
    print(pred.forecastUS_polyfit(state=state,county=county))
    print(pred.forecastUS_linreg(state=state,county=county))
    print(pred.forecastUS_prophet(state=state,county=county))

    state = "Indiana"
    county = None
    print("\n\n", county, state, "\n\n")
    print(dp.getCaseTimelineUS(state=state,county=county))
    print(pred.forecastUS_polyfit(state=state,county=county))
    print(pred.forecastUS_linreg(state=state,county=county))
    print(pred.forecastUS_prophet(state=state,county=county))

    state = "Illinois"
    county = "Cook"
    print("\n\n", county, state, "\n\n")
    print(dp.getCaseTimelineUS(state=state,county=county))
    print(pred.forecastUS_polyfit(state=state,county=county))
    print(pred.forecastUS_linreg(state=state,county=county))
    print(pred.forecastUS_prophet(state=state,county=county))

    state = "New York"
    county = "New York"
    print("\n\n", county, state, "\n\n")
    print(dp.getCaseTimelineUS(state=state,county=county))
    print(pred.forecastUS_polyfit(state=state,county=county))
    print(pred.forecastUS_linreg(state=state,county=county))
    print(pred.forecastUS_prophet(state=state,county=county))