import os
import sys
import platform

def findDir():

    path = os.getcwd()
    newPath = []

    if os.name.lower() == "posix" or platform.system() == "Darwin":
        newPath.append("/")
        path = path.split("/")
        for i in range(len(path)):
            if path[i] == "B351GroupProject":
                newPath = path[0: i]
        newPath.append("B351GroupProject")
        newPath.append("venv")
        newPath.append("Quarantina")
        newPath = "/".join(newPath)
        return newPath
    else:
        path = path.split("\\")
        for i in range(len(path)):
            if path[i] == "B351GroupProject":
                newPath = path[0:i + 1]
        newPath.append("venv")
        newPath.append("Quarantina")
        newPath = "/".join(newPath)
        return newPath



if __name__ == "__main__":
    print(findDir())
