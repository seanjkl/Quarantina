import pyttsx3
import speech_recognition as sr
import sys

engine = pyttsx3.init()

# Set properties _before_ you add things to say
engine.setProperty('rate', 150)
engine.setProperty('volume', 0.9)

r = sr.Recognizer()
mic = sr.Microphone()


class MapsAndQListener:

    ##if option is 0, then we are using this for 20q and the dictionary stores possible ways to say "yes/maybe/sometimes/no" that i could think of and returns no
    ##others (really 1) we map poi_categories to what people would typically call them in normal speak.
    def __init__(self, option):
        if option == 0:
            self.answersAsFinal = {"yes": "Y", "no": "N", "maybe": "M", "sometimes": "M", "yeah": "Y", "for sure": "Y",
                                   "totally": "Y", "definitely": "Y", "certainly": "Y", "not really": "N", "hardly": "N"}
        else:
            self.answersAsFinal = {"food":"restaurant", "im hungry":"restaurant", "i dont know":"restaurant", "surprise me": "surprise",
                                   "museum":"museum", "art":"art", "history":"museum", "shopping":"clothes", "surprise":"surprise"}


    ##function which simply waits for the user to speak, than sends it off to be translated by the Google API, stolen and repurposed from listener.py
    def getUserInput(self):
        with mic as source:
            r.adjust_for_ambient_noise(source)
            print("listening")
            audio = r.listen(source)

            message = r.recognize_google(audio)

            ##here we are checking the keys in the dictionary to avoid breaking either 20Q or openroute.
            if message in self.answersAsFinal:
                return self.answersAsFinal.get(message.lower())
            else:
                return message.lower()


    def textToSpeech(self, string):
        # print(string)
        engine.say(string)
        engine.runAndWait()


class Discard:
    ###small class used to discard logging error messages that are non valuable all credit to
    ##https://stackoverflow.com/questions/2605117/silence-loggers-and-printing-to-screen-python
    def write(self, text):
        pass

# sys.stdout = Discard()