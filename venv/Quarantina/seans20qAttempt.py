import os
import MapsAndQListener
import Interpreter
import DirectoryFinder

class TwentyqAttempt:
    ##https://www.youtube.com/watch?v=LDRbO9a6XPU methodoloy
    ##also heavily inspired by
    ##https://en.akinator.com/
    def __init__(self):
        ##only interesting thing here, is that the responses are mapped to a dictionary, and are used as index's to get either a "guess" or next question (another index)
        ##you can think of it as a 2 dimensional array, where the next question is [i][] and the y/m/n is [][j] so if the value at j ends up being 4 that tells our algo to go to line 4's data and continue
        self.guesses = 0
        self.db = []
        self.lastQuestionAsked = None
        self.finalGuess = ""
        self.answerDict = {"Y" : 3, "M" : 2, "N" : 1}
        self.listener = MapsAndQListener.MapsAndQListener(0)

        ##the exception here shouldnt ever happen
        ##but basically, csv file is loaded into self.db
        try:
            os.chdir(DirectoryFinder.findDir())
            with open("TwentyQPath.csv") as f:
                contents = f.readlines()
                for line in contents:
                    line = line.strip("\n").split(",")
                    for i in range(len(line)):
                        try:
                            line[i] = int(line[i])
                        except:
                            line[i] = line[i]
                    self.db.append(line[0::])
            print(self.db)
        except FileNotFoundError:
            print("Game cannot start - Data is absent")


    def gameStart(self):
        ##loops back and forth between asking a question, and then checking the response, input is in the form of user voice
        self.listener.textToSpeech("Welcome to 20 Questions Pandemic Edition, think of a Disease and then lets play!")
        print("Welcome to 20 Questions Pandemic Edition, think of a Disease and then lets play!")
        while self.finalGuess == "" or self.guesses <= 20:
            ##self.final guess is where the guess is stored after it is made. if it is not empty, then one has been made.
            if self.finalGuess != "":
                break
            self.askQuestion()

            inp = self.listener.getUserInput()
            self.checkResponse(inp)

        if self.guesses == 20:
            print("Darn you beat me")
            self.listener.textToSpeech("Darn you beat me")


    def askQuestion(self):
        self.guesses += 1
        if self.lastQuestionAsked == None:
            self.lastQuestionAsked = 1        ##start at 1 because i left helpful hint in CSV at 0 for how formatting works for debugging


        self.listener.textToSpeech(self.db[self.lastQuestionAsked][0])
        return self.db[self.lastQuestionAsked][0]


    def checkResponse(self, response):
        """Returns boolean on whether or not the response was enough to warrant a guess"""

        ##error checking, if the response we got was empty, something went wrong, throw a false, triggers a repeat.
        if response == "":
            return False

        answer = -1
        ##answer will be the next index of the j index the algo goes to
        ##so we get the response from the dictionary as a value and assign it to answer
        if response.upper() == "Y":
            answer = self.db[self.lastQuestionAsked][3] ##3 is yes column response
        if response.upper() == "M":
            answer = self.db[self.lastQuestionAsked][2] ##2 for maybe
        if response.upper() == "N":
            print(self.db[self.lastQuestionAsked])
            answer = self.db[self.lastQuestionAsked][1] ##1 for No

        ##the answer can be either a int that points to the next question in the lists, or it can be a string, which is the bots guess
        ##two options can happen here, we can get an int, which means we need to go back and ask another question
        ##or we got a string, which means the Algo believes it has reached a solution.
        if isinstance(answer, int):
            self.lastQuestionAsked = answer
            return False                      ##false to indicate we are still going
        else:
            self.makeGuess(answer)
            print(self.finalGuess)
            self.listener.textToSpeech(self.finalGuess)
            self.checkFinalGuess(response, answer)
            return True                      ##true to indicate we think we are done

    def checkFinalGuess(self, originalResponse, originalAnswer):
        ##this simply checks whether or not the AI was correct in an attempt to help it learn for the future.
        ##when the answer is no a new path is added to the algo.
        print("Was I right")
        self.listener.textToSpeech("Was I right?")
        # response = input("What i right? y n")

        recognized = False
        while (not recognized):
            try:
                response = self.listener.getUserInput()
                recognized = True
            except:
                pass

        if response.lower() == "n":
            self.addNewPath(originalResponse, originalAnswer)
        else:
            self.listener.textToSpeech("Thanks for playing, better luck next time")

    ##function just adds one to guesses and returns a string for guess
    def makeGuess(self, answer):
        ##simple function, adds 1 to guesses, returns finalGuess as a textToSpeech friendly output
        self.guesses += 1
        self.finalGuess = "Oh, I know its " + answer
        return self.finalGuess

    def isWinner(self):
        ##not used
        if self.guesses > 20:
            return True
        if self.finalGuess != "":
            return True

    def reWrite(self):

        ##convert ints back to strings
        for i in range(len(self.db)):
            for j in range(len(self.db[i])):
                self.db[i][j] = str(self.db[i][j])

        ##reopen the old file)
        file = open("TwentyQPath.csv", "w")
        lst = []

        for line in self.db:
            newLine = ",".join(line)+"\n"
            lst.append(newLine)

        ##dump the contents of db
        file.writelines(lst)
        file.close()

        ##kindly inform the user you are not salty that you were defeated
        print("Fuck you dont play again")
        self.listener.textToSpeech("Fuck you dont play again")

    def addNewPath(self, response, answer):
        responseToCoherent = {"Y": "True", "N":"False", "M":"Sometimes True and Sometimes False"}

        ##we only get here if we were wrong, so we ask the user to tell us what it was
        print("Darn youre good what was it?")
        self.listener.textToSpeech("Darn youre good what was it?")
        newAnswer = self.listener.getUserInput()

        ##debug used to get input so we dont use too many of the google translates
        # newAnswer = "Chlymidia"

        self.db[self.lastQuestionAsked][self.answerDict.get(response.upper())] = len(self.db)

        ##now we ask them to give us questions that will help build new paths to get us here in the future.
        print("Help me get better enter a new question!")
        self.listener.textToSpeech("Help me get better enter a new question!")
        print("I need a question that is " + responseToCoherent.get(response.upper()) + " for " + answer + " but not for "  + newAnswer)
        self.listener.textToSpeech("I need a question that is " + responseToCoherent.get(response.upper()) + " for " + answer + " but not for "  + newAnswer)
        newQ = self.listener.getUserInput()
        # newQ = input("Enter a Q thats " + response + " for " + answer + " but not for "  + newAnswer)
        self.db.append([newQ,answer,len(self.db) + 1,newAnswer])

        ## another clarifying questions
        print("Now, tell me a question that is sometimes true and sometimes false for" + answer + " but not for " + newAnswer)
        self.listener.textToSpeech("Now, tell me a question that is sometimes true and sometimes false for" + answer + " but not for " + newAnswer)
        newMQ = self.listener.getUserInput()
        # newMQ = input("Enter a Q that is sometimes y and sometimes n for" + answer + " but not for " + newAnswer)

        ##add it to the db
        self.db.append([newMQ, answer, answer, newAnswer])

        ##and then rewrite it
        self.reWrite()


if __name__ == "__main__":
    # listen = TwentyQListener.TwentyQListener()
    # print("finishes")
    # print(listen.getUserInput())
    Twentyq = TwentyqAttempt()
    Twentyq.gameStart()
