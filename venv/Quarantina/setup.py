import json
import pgeocode
import pyttsx3
import os
import DirectoryFinder

# One time initialization
engine = pyttsx3.init()

# Set properties _before_ you add things to say
engine.setProperty('rate', 150)
engine.setProperty('volume', 0.9)

def run():

    engine.say("Hi! I'm Quarantina. Good to meet you, what's your first name?")
    engine.runAndWait()
    first_name = input()

    engine.say(f"Good to meet you {first_name}. And your last name?")
    engine.runAndWait()
    last_name = input()

    engine.say("Cool, cool. Last thing, what's your zipcode?")
    engine.runAndWait()
    last_name = input()

    engine.say("And can I get your street address?")
    engine.runAndWait()
    address = input()

    engine.say("Great! Setup complete. Push the button if you want to talk.")
    engine.runAndWait()


    data = {}
    data["first_name"] = first_name
    data["last_name"] = last_name
    data["zipcode"] = "47408"
    data["address"] = address

    nomi = pgeocode.Nominatim('us')
    df = nomi.query_postal_code(data["zipcode"])
    county = df.loc['county_name']
    state = df.loc['state_name']
    city = df.loc['place_name']

    data["city"] = city
    data["county"] = county
    data["state"] = state
    data["address"] = address

    del nomi, df

    os.chdir(DirectoryFinder.findDir())
    outfile = open('user_data.json', 'w')
    json.dump(data, outfile)
    outfile.close()