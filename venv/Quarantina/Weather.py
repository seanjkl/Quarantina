from bs4 import BeautifulSoup
from bs4.element import NavigableString
import csv
import datetime as dt
import os
import requests
import json
import pgeocode


class Weather():

    import DirectoryFinder as df

    def __init__(self,zipcode=None):
        os.chdir(df.findDir())
        if zipcode is None:
            with open('user_data.json') as f:
                data = json.load(f)
                self.zipcode = data["zipcode"]
        else:
            self.zipcode = zipcode


        nomi = pgeocode.Nominatim('us')
        df = nomi.query_postal_code(self.zipcode)
        self.lat = df.loc['latitude']
        self.lon = df.loc['longitude']

        del df, nomi

    def getCurrentWeather(self):

        url = f'https://forecast.weather.gov/MapClick.php?lat={self.lat}&lon={self.lon}'
        page = requests.get(url)

        soup = BeautifulSoup(page.text, 'html.parser')

        condition = soup.find("p", class_="myforecast-current").contents[0]

        temp = soup.find("p", class_="myforecast-current-lrg").contents[0][:-2]

        return f"It is currently {temp} degrees Fahrenheit and {condition}"

    def getTodaysWeather(self):

        url = f'https://forecast.weather.gov/MapClick.php?lat={self.lat}&lon={self.lon}'
        page = requests.get(url)

        soup = BeautifulSoup(page.text, 'html.parser')

        details = soup.find(class_="row row-odd row-forecast").contents
        d1 = str(details[0].contents[0].contents[0])
        d2 = str(details[1].contents[0])

        details = soup.find(class_="row row-even row-forecast").contents
        d3 = str(details[0].contents[0].contents[0])
        d4 = str(details[1].contents[0])

        return f"{d1} it is {d2}{d3} it will be {d4}"

    def getTomorrowsWeather(self):

        url = f'https://forecast.weather.gov/MapClick.php?lat={self.lat}&lon={self.lon}'
        page = requests.get(url)

        soup = BeautifulSoup(page.text, 'html.parser')

        data = soup.findAll(class_="forecast-tombstone")

        tomorrow = dt.date.today() + dt.timedelta(days = 1)
        tomorrow = tomorrow.strftime('%A')

        tomorrow_string = ''
        for card in data:
            if tomorrow in str(card):
                tag = card.img
                tomorrow_string += tag['alt']

        tomorrow_string = tomorrow_string.replace('  ', ' ')
        tomorrow_list = tomorrow_string.split('. ')
        tomorrow_list = [i for i in tomorrow_list if not ('mph' in i or 'inch' in i)]
        tomorrow_string = '. '.join(tomorrow_list)
        
        return tomorrow_string

if __name__ == "__main__":
    w = Weather()
    # w.getCurrentWeather()
    print(w.getTomorrowsWeather())