import speech_recognition as sr
from Interpreter import *
import pyttsx3
import setup
import Maps
import warnings
import sys
import DirectoryFinder as df

if not sys.warnoptions:
    warnings.simplefilter("ignore")

def print_and_say(string):
    print(string)
    engine.say(string)
    engine.runAndWait()

# One time initialization
engine = pyttsx3.init()

# Set properties _before_ you add things to say
engine.setProperty('rate', 150)
engine.setProperty('volume', 0.9)

r = sr.Recognizer()

mic = sr.Microphone()

class Listener:

    def __init__(self):
        try:
            os.chdir(df.findDir())
            f = open("user_data.json", "r")
            self.setup = True
            f.close()
        except FileNotFoundError:
            self.setup = False

        if self.setup == False:
            setup.run()
            self.setup = True

        try:
            with mic as source:
                r.adjust_for_ambient_noise(source)
                print_and_say("Listening...")
                audio = r.listen(source)

                message = r.recognize_google(audio)
                print(message)


                interpretation = interpret(message)
                print_and_say(interpretation)
        except:
            print_and_say("it seems there was an error")

    def check_state(self):
        return True

if __name__ == "__main__":
    test = Listener()


