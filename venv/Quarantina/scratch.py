import random

synonyms = {
    'covid':     ['covid','corona','coronavirus','pandemic'],
    'weather':   ['weather','forecast'],
    'streaming': ['show','movie','shows','movies','watch','bored','entertainment','entertain','streaming'],
    'jokes':     ['joke','jokes','bored','comedy','entertainment','entertain'],
    'twentyq':   ['bored','game','twenty questions', 'questions', 'twenty','q\'s', '20 questions', '20 q', '20 questions','Twenty questions','twentyq','twenty q', '20q', "20", "q"],
    'maps':      ['route','outside','food','hungry','resturaunt','museum','walk','bored','workout','work out','exercise','fresh air', "maps"],
    'jokes':     ['jokes', 'joke', 'laugh', 'funny', 'giggle', 'one liner', 'jest', 'quip', 'pun'],
    'other': [],
}

message = ['im', 'bored']

counts = {
    'covid': 0,
    'covid_update': 0,
    'weather': 0,
    'streaming': 0,
    'jokes': 0,
    'twentyq': 0,
    'maps': 0,
    'other': 0,
}

# heuristic
max_key = 'other'
max_val = 0
for key,val in synonyms.items():
    for i in message:
        if i in val:
            counts[key] += 1
    if counts[key] > max_val:
        max_key = key
        max_val = counts[key]
    elif counts[key] == max_val:
        rand = random.randint(0,1)
        max_key = key if rand == 0 else max_key

print(max_key)