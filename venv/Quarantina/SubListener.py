import pyttsx3
import speech_recognition as sr
import sys

class SubListener:

    def __init__(self):
        self.engine = pyttsx3.init()

        # Set properties _before_ you add things to say
        self.engine.setProperty('rate', 150)
        self.engine.setProperty('volume', 0.9)

        self.r = sr.Recognizer()
        self.mic = sr.Microphone()

    ##function which simply waits for the user to speak, than sends it off to be translated by the Google API, stolen and repurposed from listener.py
    def getUserInput(self):
        with self.mic as source:
            self.r.adjust_for_ambient_noise(source)
            print("listening")
            audio = self.r.listen(source)

            message = self.r.recognize_google(audio)

            return message.lower()


    def textToSpeech(self, string):
        # print(string)
        self.engine.say(string)
        self.engine.runAndWait()


class Discard:
    ###small class used to discard logging error messages that are non valuable all credit to
    ##https://stackoverflow.com/questions/2605117/silence-loggers-and-printing-to-screen-python
    def write(self, text):
        pass

# sys.stdout = Discard()