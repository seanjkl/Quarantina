import openrouteservice as opr
import os
import socket
import geocoder
import requests
import pygeoj
import json
import random
import MapsAndQListener
import DirectoryFinder as df
from geopy.geocoders import Nominatim
from math import radians, cos, sin, asin, sqrt


##token for openroute 5b3ce3597851110001cf6248d4d140238ffa4b8a84822f555e9256be
##https://openrouteservice.org/dev/#/api-docs/introduction
##Most code has been heavily modified from the openrouteservice API Documentation
##https://geocoder.readthedocs.io/


class Maps:
    categories = ["restaurant", "museum", "art", "clothes", "parking"]
    key = "5b3ce3597851110001cf6248d4d140238ffa4b8a84822f555e9256be"
    headers = {
        'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
        'Authorization': '5b3ce3597851110001cf6248d4d140238ffa4b8a84822f555e9256be',
        'Content-Type': 'application/json; charset=utf-8'
    }

    def __init__(self):
        self.currentLocation = self.userLocation()
        self.listener = MapsAndQListener.MapsAndQListener(1)



    def userLocation(self):
        """ returns latitude longitutde of device **City as of now no exact measurements without GPS"""
        try:
            os.chdir(df.findDir())
            ##gather and stylize data from the json for the request from API
            ##due to the finicky nature of OpenRouteSerivce, we had to abandon using address, as if the lat/long does not appear on a street or building (e.g. someones lawn)
            ##the API freaks out and throws the user into the ocean off the coast of africa, which is obviously not ideal, so too avoid intense error checking we are sticking with
            ##IP based location which although inaccurate, is the best option.
            with open('user_data.json') as f:
                data = json.load(f)
                self.address = data["address"]+","
                self.city = data["city"]+","
                self.county = data["county"]
                self.state = data["state"]

            geolocator = Nominatim(user_agent="Quarantina")
            location = geolocator.geocode(self.address + " " + self.city)
            # print(location.raw)
            # currentlocation = self.reverseAddressLookup()
            # if currentlocation == None:
            currentlocation = geocoder.ip('me')
            return currentlocation.latlng
            # return[round(location.latitude,4), round(location.longitude, 4)]

        except FileNotFoundError:
            currentlocation = geocoder.ip('me')
            return currentlocation.latlng


    def getPOIs(self):
        """updates the POI's JSON file, returns true upon completion"""

        ###This is is a similar structure as all request to the OpenRouteService API require, the body is simply comprosied of a bounding box with a starting point and ending point (2-d plane) that will
        ### be scanned for points of interests and returned)
        ### interesting note here, is that it returns lat , long but all the arguments always want the form in long , lat.

        body = {"request": "pois", "geometry": {"bbox": [[self.currentLocation[1] + .01, self.currentLocation[0] -.01], [self.currentLocation[1] - .01, self.currentLocation[0] + .01] ],
                                                "geojson": {"type": "Point", "coordinates": [self.currentLocation[1], self.currentLocation[0]]},
                                                "buffer": 200}}
        # -86.531625
        call = requests.post('https://api.openrouteservice.org/pois', json=body, headers=Maps.headers)

        ### some of the functions depend on the file being written, as I wrote them at 3 am and I am unsure what i would break if i tried to go back and change it
        f = open("MapJSONS/tempPOIS.json", "w+")
        f.writelines(call.text)
        f.close()
        return call.text


    def filterPOIS(self, criteria):
        """ filters out the nearby POIS based on a feature such as 'restaurant' or 'parking' etc"""
        points = pygeoj.load("MapJSONS/tempPOIS.json")


        ##create the dictionary that will store the things we care about in the POI's nearby
        ## and turn the json into a dictionary
        ## {name : (long, lat)}
        dic = {}
        for feature in points:

            ##this stores info about the POI being a restauranmt or historic site / parking etc
            finalVal = [feature.geometry.coordinates]
            categoryInfo = list(dict(list(feature.properties.values())[3]).values())

            for entry in categoryInfo:
                for val in entry.values():
                    finalVal.append(val)

            dic[dict(list(feature.properties.values())[4]).get("name")] = finalVal


        ## take that dictionary and filter out based on provided arg
        newDic = {}
        for entry in dic.items():
            # print(entry[1][1])
            if entry[1][1].lower() == criteria.lower():
                newDic[entry[0]] = entry[1]

        return newDic


    def getNames(self, listOfPoints):
        ##this function makes a request to get the names of all points provided.
        ##it takes for example a [long, lat] and returns what it is, so if you know the long/lat of qdoba you can find out all the other information regarding it.
        ## returns a GEOJson
        headers = {
            'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
        }

        if (len(listOfPoints) >= 3):
            for point in listOfPoints[0:-2]:
                lon = point[0]
                lat = point[1]
                link = "https://api.openrouteservice.org/geocode/reverse?api_key=5b3ce3597851110001cf6248d4d140238ffa4b8a84822f555e9256be&point.lon=" + str(lon) + "&point.lat=" + str(lat)
                # call = requests.get(link, headers=headers)
                #
                # print(call.text)
                return call.text
        else :
            for point in listOfPoints:
                lon = point[0]
                lat = point[1]
                call = requests.get(
                    "https://api.openrouteservice.org/geocode/reverse?api_key=5b3ce3597851110001cf6248d4d140238ffa4b8a84822f555e9256be&point.lon=" + str(
                        lon) + "&point.lat=" + str(lat),
                headers = headers)

                return call.text


    def getWalkingPath(self, filter = None):
        ##documentation for pygeoj was found https://gis.stackexchange.com/questions/73768/converting-geojson-to-python-objects

        """returns a walking path from users current location (Destination Name, [lat long], total distance, total time, directions(rawJson)]"""
        # current = self.parseJson(city, state)   -- BROKEN
        pois = self.getPOIs()

        ##used to throw in a category to filter out POI's not in that category
        if filter != None:
            pois = self.filterPOIS(filter)

        pois = json.loads(pois)["features"]

        ##POIDetail stores (Name of place : (["Category General Type", "Category Fine Type"], [Lat, Long])
        ##useful for later adding more functionality
        poiDetail = {}
        # print(pois)
        for entry in pois:

            #We break down the GeoJson object recieved to get only the name of the location and the long lat as well as what category it is a part of
            ## the category is only useful (since presumably we have filtered or dont care) so that the text to speech sounds more fluid

            name = entry.get("properties").get("osm_tags").get("name")
            point = entry.get("geometry").get("coordinates")

            for catDetail in list(entry.get("properties").get("category_ids").values()):
                details = list(catDetail.values())

            poiDetail[name] = (details, point)

        ##Breaking down the json and pruning out unneccesary info
        choice = random.choice(list(poiDetail.keys()))
        choiceCords = poiDetail.get(choice)[1]
        path = json.loads(self.generateWalkingPath(choiceCords))["features"]
        path = path[0].get("properties").get("segments")
        path = path[0]

        ##Pruning out totals from JSON, total distance, duration, and steps
        totalDistance = path.get("distance")
        totalDuration = path.get("duration")
        steps = path.get("steps")


        return (choice, poiDetail.get(choice)[1], totalDistance, totalDuration, steps)


    def generateWalkingPath(self, destination):
        ##Stadium and 20th street crosswalk is 39.181009,-86.528365
        ##Gresh dining hall is 39.174480,-86.518892
        #39.17898, -86.528406 ## [long, lat]

        ##like other requests, this function simply queries the API for directions form point a (which is taken from the users location) to a provided location)
        body = {"coordinates": [[self.currentLocation[1], self.currentLocation[0]], [destination[0], destination[1]]]}

        call = requests.post('https://api.openrouteservice.org/v2/directions/foot-walking/geojson', json=body, headers=Maps.headers)

        #print(call.status_code, call.reason)
        return call.text

    def getWalkingPathDirection(self, walkingPath):
        ##Another utitlity function for cleaning the massive GeoJson's, this one only returns the totalDistance, TotalDuration and initial direction to be traveled on said route
        ## example (104 (meters), 164 (minutes), "Head Northwest")
        son = json.loads(walkingPath)
        son = son["features"]
        son = son[0]
        totalDistance, totalDuration = son.get("properties").get("segments")[0].get("distance"), son.get("properties").get("segments")[0].get("duration")
        direction = (son.get("properties").get("segments")[0].get('steps')[0].get('instruction'))
        return (totalDistance, totalDuration, direction)


    def getLatLocationLatLong(self, city, state):
        ##queries the api to reverse lookup the users lat long based off city and state information.
        ##this has been depreciated as IP proved to be more accurate
        """Takes a location in string supplied by user and returns when only one choice [long, lat] or returns list containing all choices"""
        headers = {
            'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
        }

        site = "https://api.openrouteservice.org/geocode/autocomplete?api_key=" + Maps.key + "&text=" + city + " " + state
        call = requests.get(site, headers=headers)

        ##check for bad requests, print error message debug only
        if(call.status_code != 200):
            print(call.status_code, call.reason)


        ##Feature collection of the json
        return json.loads(call.text)['bbox']

    def getCurrentLocation(self):
        return self.currentLocation

    def getSurpriseTrip(self):
        ##this function looks at all pois in the near vicinity, cleans them up, and picks one and sends back its (name, lat, long, category)
        ##ex: ("Penn Station", 48.32654, -86.31264, "restaurant")
        son = json.loads(self.getPOIs())["features"]
        entries = []
        for entry in son:
            name = entry.get('properties').get('osm_tags').get('name')
            if name != None:
                cordinates = entry.get('geometry').get('coordinates')

                category = list(entry.get('properties').get('category_ids').values())[0].get("category_name")
                entries.append((name, cordinates[0], cordinates[1], category))

        return entries

    def runMaps(self):
        ##Main function interacts with the listener to allow for the user to interact with their voice with the rest of the API

        print("What are you in the mood for today? You can say things like: food, history, shopping or surprise me")
        self.listener.textToSpeech("What are you in the mood for today? You can say things like: food, history, shopping or surprise me")

        ##save on google voice translations use input for debugging
        response = self.listener.getUserInput()

        ##since we first asked the user if they had a preferece, we check that preference and if they have none, we can call the getSurpriseTrip function to find us one
        if response == "surprise":
            print("Picking a special trip for you")
            self.listener.textToSpeech("Picking a special trip for you")
            path = self.getSurpriseTrip()
            path = random.choice(path)
            walkingPath = self.generateWalkingPath((path[1], path[2]))
            walkingPath = self.getWalkingPathDirection(walkingPath)
            print("How about a walk to a " + path[3] +"?  I was thinking " + path[0] + " it will take only " + str(walkingPath[0]) + "minutes round trip and you will walk "\
                                       + str(walkingPath[1]) + " meters to begin " + walkingPath[2])
            self.listener.textToSpeech("How about a walk to a " + path[3] +"?  I was thinking " + path[0] + " it will take only " + str(walkingPath[0]) + "minutes round trip and you will walk "\
                                       + str(walkingPath[1]) + " meters to begin " + walkingPath[2])

        ##otherwise we need to filter out our POI's based on what they are in the mood for.
        else:
            pois = self.getPOIs()             ## this line, although appearing unused is actually called to rewrite the json
            pois = self.filterPOIS(response)

            try:
                ##I chose to only show the 5 closest results (or <5 catch the error)
                if len(list(pois.keys())) == 1:
                    print("Here is the closest " + response + " to you")
                    self.listener.textToSpeech("Here is the closest " + response +" to you")
                else:
                    print("Here are the closest 5 " + response + "'s to you")
                    self.listener.textToSpeech("Here are the closest 5 " + response + "'s to you")
                choices = list(pois.keys())[0:4]
            except IndexError:
                # print("index")
                if len(list(pois.keys())) == 1:
                    print("Here is the closest " + response + " to you")
                    self.listener.textToSpeech("Here is the closest " + response +" to you")
                else:
                    print("Here are the closest " + response +"'s to you")
                    self.listener.textToSpeech("Here are the closest " + response +"'s to you")
                choices = list(pois.keys())       ##we convert the pois to keys because we only care about the names of the locations for relaying that info to the user (lat,long,category) is not as useful

            ##give the user their choices
            for choice in choices:
                print(choice)
                self.listener.textToSpeech(choice)
                # print(choice)
            self.listener.textToSpeech("Which place would you like to visit?")

            ##force them to pick
            response = self.listener.getUserInput().title()

            print("Calculating distance and time spent walking")
            self.listener.textToSpeech("Calculating distance and time spent walking")

            choice = pois.get(response.title())

            ##after dialogue, we get path from the user to the destination, and then the formatted information of the directions
            path = self.generateWalkingPath(choice[0])
            path = self.getWalkingPathDirection(path)

            ## I had to cut out the specific directions for a couple of big reasons, first since we dont have GPS capability on the PI our distance is limited to cardinal directions at best, Second, when doing
            ## text to speech listening to full fledged directions in *not* real time is not useful, and annoying.
            print("Your walk to " + response + " will take approximately " + str(path[0]) + " minutes round trip and you will walk " + str(path[1]) + " meters to begin please" + path[2])
            self.listener.textToSpeech("Your walk to " + response + " will take approximately " + str(path[0]) + " minutes round trip and you will walk " + str(path[1]) + " meters to begin please " + path[2])

        ##dont forget to bring a towel
        print("The CDC recommends you wear a face covering in public and maintain a strict distance of 6 feet between others. Help slow the spread so i dont graduate into a depression.")
        self.listener.textToSpeech("The CDC recommends you wear a face covering in public and maintain a strict distance of 6 feet between others. Help slow the spread so i dont graduate into a depression.")

    def reverseAddressLookup(self):
        ##oh this just takes an address and formats it so that the OpenRouteService doesnt throw a fit,
        ##it returns lat long
        ##its been depreciated as the openrouteservice API does not like when addresses are shared residences (e.g.) apartments
        ##as it puts the pointer in weird locations and then the default exception on their back end (which we have no control over)
        ## is to throw that location off the coast off africa

        headers = {
            'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
        }
        callString = self.address +" " + self.city + " "+ self.state
        # print(callString)
        callString = callString.split(" ")
        callString = "%20".join(callString)
        # print(callString)
        call = requests.get(
            'https://api.openrouteservice.org/geocode/autocomplete?api_key=5b3ce3597851110001cf6248d4d140238ffa4b8a84822f555e9256be&text=' + callString,
            headers=headers)

        if call.status_code != 200:
            return None
        else:
            son = json.loads(call.text)["features"]
            cordinates = son[0].get("geometry").get("coordinates")
            cordinates = [cordinates[1], cordinates[0]]
            return cordinates

if __name__ == "__main__":
    test = Maps()
    # testPoint1 = [-86.528406, 39.17898]
    # testPoint2 = [-86.518892, 39.174480]
    # print(test.getCurrentLocation())
    # print(test.getPOIs())
    # print(test.filterPOIS())
    test.runMaps()

    # print(test.getWalkingPath("Bloomington", "Indiana", "restaurant"))
    # print(test.getWalkingPath())
    # print(test.getWalkingPath("Bloomington", "Indiana"))


