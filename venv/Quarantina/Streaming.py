import pandas as pd
import numpy as np
import difflib
from SubListener import *
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sys import argv
import DirectoryFinder as df
import os

# given_movie_name = "spdr mn"

#script, given_movie_name = argv
listener = SubListener()

# read the csv file of with all the movie data
os.chdir(df.findDir())
data = pd.read_csv('tmdb_5000_movies_with_cast.csv')

#   printing the first 3 rows to make sure that the cast column was concatenated properly (this was done in a different file)
# print(data.head(3))

# list of relevant columns
key_columns = ['genres', 'keywords', 'production_companies', 'title']

#   print the data set with only the relevant columns
# print(data[key_columns].head(3))

# concatenate all the data in the relevant columns to get a column which we can use
# to evaluate similarities between movies
for i in key_columns:
    data[i] = data[i].fillna('')  # fill any empty slots to aid in concatenation

# in order to sequence match we have to manipulate lists, so we need to convert the panda series to a list
titles_list = data['title'].tolist()

edited_titles_list = {}

# we need to take all the titles, enumerate them into the dictionary edited_titles_list so we have a
# lowercase title, and an index to reference.
# the reason I lower cased all the titles, is because I find that the SequenceMatcher function works better
# when all the characters being compared are the same case.

for index, value in enumerate(titles_list):
    edited_titles_list[index] = value.lower()


# function which matches input to the closest movie title
def error_correction(given_movie_name):
    matches = {}

    # matches is a dictionary whose key is the index of the movies, and it's value will be a score of how similar the movie
    # title for that index is to the given_movie_name. The for loop runs through the edited_titles_list, gets the lowercase title
    # compares it to the given_movie_name, scores how similar they are and puts that score in the appropriate index in
    # the dictionary matches.

    for i in edited_titles_list:
        present_movies = edited_titles_list.get(i)

        similarity = difflib.SequenceMatcher(isjunk=None, a=given_movie_name, b=present_movies).ratio()

        matches[i] = similarity

    # print(matches)

    # returns a tuple of highest score (best match), and it's index
    max_value = max(zip(matches.values(), matches.keys()))

    # retrieves the index of the closest movie from max value
    max_key = max_value[1]

    # returns a tuple of a string containing the lowercase title, and an integer representing its index
    return edited_titles_list.get(max_key), max_key


def concatenated_column(row):
    return row['genres'] + " " + row['keywords'] + " " + \
           row['production_companies'] + " " + row['title']


data['concatenated_column'] = data.apply(concatenated_column, axis=1)

# print(data.head(3))

#creates a matrix with a token count for each word, for each movie, in the concatenated_column. 
concatenated_matrix = CountVectorizer().fit_transform(data["concatenated_column"])

#compares the cosine angle of the vectors in the concatenated matrix in order to measure their similarity
similarity_matrix = cosine_similarity(concatenated_matrix)


# print(similarity_matrix)

#   Now we'll make a functions that'll help us get the name of the movie from an index When given a movie,

def title_from_index(index):
    return data[data.index == index]['title'].values[0]


def recommend_movie(movie_name):
    # first we take the movie_name string we're given and generate a tuple of the closest match and the index
    correction = error_correction(movie_name)

    movie_name = correction[0]  # movie name retrieves the string from the tuple 'correction'

    movie_index = correction[1]  # movie index retrieves the index from the tuple 'correction'

    # print(test_index)

    #   We'll take the given movie,using the similarity matrix, we'll make a list
    #   containing tuples of the movie indices, and their similarity relative to the given movie.

    similar_movies = list(enumerate(similarity_matrix[movie_index]))

    # print(similar_movies)

    #   sort based on how similar the movies are
    sorted_similar_movies = sorted(similar_movies, key=lambda x: x[1], reverse=True)[1:]

    # print(sorted_similar_movies)

    i = 0

    output = "Based on the movie " + movie_name + " the top 10 most similar movies are:"

    for x in sorted_similar_movies:
        output += title_from_index(x[0]) + "\n"
        i = i + 1
        if i >= 10:
            break

    return output

def streaming():
    # textToSpeech("Would you like a recommendation based on genre or a movie you like?")
    # user_input = getUserInput()

    # if 'genre' in user_input:
    #     textToSpeech("What genre are you in the mood for?")
    #     genre = getUserInput()
    # else:
    listener.textToSpeech("What is a movie you like?")
    # user_input = getUserInput()
    user_input = listener.getUserInput()
    return recommend_movie(user_input)


if __name__ == "__main__":
    print(streaming())
