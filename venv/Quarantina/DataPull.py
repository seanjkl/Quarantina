import subprocess
import os
import numpy as np
from datascience import *
import datetime
import matplotlib
import pandas as pd
from datetime import datetime
from datetime import timedelta
import pathlib
import warnings
warnings.filterwarnings("ignore")

class DataPull:
    
    def __init__(self, load_data=True):
        # path = os.getcwd().split("\\")
        path = str(pathlib.Path(__file__).parent.absolute()).split("\\")
        ##get the entire system path, except the last two folders (venv, quarantina) and add Data
        self.path = "\\".join(path)
        self.localPath = "\\".join(path[0:-2])+"\\Data"
        self.remotePath = "https://github.com/CSSEGISandData/COVID-19.git"
        self.rawData = np.array(0)
        if load_data == True: self.loadRawData()

    def genericCommand(self, *args):
        os.chdir(self.localPath)
        return subprocess.check_call(['git'] + list(args))

    def clone(self):
        subprocess.Popen(['git', 'clone', self.remotePath, self.localPath])


    def run(self, command):
        if command.lower() == "update" or command.lower() == "pull":
            self.genericCommand("pull", "origin", "master")
        elif command.lower() == "clone":
            self.clone()
        else:
            print("wrong command")

    def loadRawData(self):
        os.chdir(self.localPath)
        os.chdir("csse_covid_19_data")
        os.chdir("csse_covid_19_daily_reports")

        files = os.listdir(os.getcwd())

        ##newest file as sorted by windows
        data = files[-2]
        self.rawData = Table.read_table(data)

        os.chdir(self.path)



    ## Get dictionary  of {State : Active Infections}
    def getActiveInfectionsByState(self):
        """ returns a dictionary of {State : Active Infections} """
        infectDict = {}
        data = self.rawData

        ##Get raw data, and clean it up for just US States
        ##sum up active cases
        data = Table().with_columns("State", data[2],
                                  "Country", data[3],
                                  "Cases", data.column("Confirmed"))
        data = data.where("Country", are.equal_to("US"))
        data = data.group("State", sum)
        data = Table().with_columns("State", data.column(0),
                                    "State Active Cases", data.column(2))

        states = data[0]
        cases = data[1]
        for i in range(len(states)):
            infectDict[states[i]] = cases[i]
        return infectDict

    def getCurrentCases(self,state=None,county=None,country=None):
        
        if state: state = state.lower().capitalize()
        if county: county = county.lower().capitalize()

        path = os.getcwd()
        
        if state and county:
            filename = path + r"\Data\csse_covid_19_data\csse_covid_19_time_series\time_series_covid19_confirmed_US.csv"
            df = pd.read_csv(filename)
            cols = df.keys()

            county_cases = df.loc[df['Province_State'] == state].loc[df['Admin2'] == county].reset_index().values[:,-1][0]

            return county_cases 
        
        elif state:
            return self.getActiveInfectionsByState()[state]

        elif country:
            return self.getCountryCases()[country]

        else:
            return self.getCountryCases()['US']


    ## Get dictionary  of {State : Deaths}
    def getDeathsByState(self):
        """
        returns a dictionary of {State : Deaths}
        """
        deathDict = {}
        data = self.rawData

        ##Get raw data, and clean it up for just US States
        ##sum up active cases
        data = Table().with_columns("State", data[2],
                                    "Country", data[3],
                                    "Deaths", data.column("Deaths"))
        data = data.where("Country", are.equal_to("US"))
        data = data.group("State", sum)
        data = Table().with_columns("State", data.column(0),
                                    "Deaths", data.column(2))

        states = data[0]
        cases = data[1]
        for i in range(len(states)):
            deathDict[states[i]] = cases[i]
        return deathDict


    def getRecoveriesByState(self):
        """ Returns a Dictionary of {State : Recoveries}, Likely all 0, reporting is sparse """
        stateRecoveries = {}
        data = self.rawData
        

        data = Table().with_columns("State", data[2],
                                    "Country", data[3],
                                    "Recoveries", data.column("Recovered"))
        data = data.where("Country", are.equal_to("US"))
        data = data.group("State", sum)
        data = Table().with_columns("State", data.column(0),
                                    "Recoveries", data.column(2))
        print(data)
        for i in range(len(data[1])):
            if (data[1][i] != 0):
                stateRecoveries[data[0][i]] = data[1][i]

        return stateRecoveries


    def getCountryCases(self):
        """ Returns a dictionary of {Country : Active Infections}"""
        countryCases = {}
        data = self.rawData

        data = Table().with_columns("Country", data[3], "Cases", data.column("Confirmed"))
        data = data.group("Country", sum)
        for i in range(len(data[1])):
            countryCases[data[0][i]] = data[1][i]
        return countryCases

    def getCountryDeaths(self):
        """ Returns a dictionary of {Country : Deaths} """
        countryDeaths = {}
        data = self.rawData

        data = Table().with_columns("Country", data[3],
                                    "Deaths", data.column("Deaths"))
        data = data.group("Country", sum)
        for i in range(len(data[1])):
            countryDeaths[data[0][i]] = data[1][i]

        return countryDeaths

    def getCountryRecoveries(self):
        """ Returns a dictionary of {Coutnry : Recoveries}"""
        countryRecoveries = {}
        data = self.rawData

        data = Table().with_columns("Country", data[3],
                                    "Recoveries", data.column("Recovered"))
        data = data.group("Country", sum)
        for i in range(len(data[1])):
            if(data[1][i] != 0):
                countryRecoveries[data[0][i]] = data[1][i]

        return countryRecoveries


    def getCaseTimelineUS(self, state=None, county=None):

        """ Returns a list of cases with index as days since 1/22/2020 """

        timeline = []
        if state and state != "New York": state = state.lower().capitalize()
        if county and county != "New York": county = county.lower().capitalize()

        path = os.getcwd()
        
        
        if state and county:
            filename = self.localPath + r"\csse_covid_19_data\csse_covid_19_time_series\time_series_covid19_confirmed_US.csv"
            df = pd.read_csv(filename)
            cols = df.keys()

            county_lookup = df.loc[df['Province_State'] == state].loc[df['Admin2'] == county].reset_index()

            for col in cols[11:]: timeline.append(county_lookup.get_value(0,col))
        
        elif state:
            filename = self.localPath + r"\csse_covid_19_data\csse_covid_19_time_series\time_series_covid19_confirmed_US.csv"
            df = pd.read_csv(filename)
            cols = df.keys()

            state_lookup = df.loc[df['Province_State'] == state].reset_index()

            for col in cols[11:]:
                state_sum = 0
                for index, row in state_lookup.iterrows(): state_sum += int(row[col])
                timeline.append(state_sum)

        else:
            filename = self.localPath + r"\csse_covid_19_data\csse_covid_19_time_series\time_series_covid19_confirmed_global.csv"
            df = pd.read_csv(filename)
            cols = df.keys()

            US = df.loc[df['Country/Region'] == 'US'].reset_index()

            for col in cols[4:]: timeline.append(US.get_value(0,col))

        return timeline

    def getCountyCasesYesterday(self,state,county):

        d = {
            'yesterday':0,
            'today':0,
        }

        path = os.getcwd()
        filename = self.localPath + r"\csse_covid_19_data\csse_covid_19_time_series\time_series_covid19_confirmed_US.csv"
        df = pd.read_csv(filename)

        cols = df.keys()

        county_lookup = df.loc[df['Province_State'] == state].loc[df['Admin2'] == county].reset_index()
        
        most_recent_update = county_lookup.get_value(0,cols[-1])
        second_recent_update = county_lookup.get_value(0,cols[-2])

        return (most_recent_update, second_recent_update)

if __name__ == "__main__":
    test = DataPull(load_data=True)
    test.run("pull")

    # print(test.getCountryRecoveries())
    # print(test.getCaseTimelineUS(state="indiana",county="Monroe"))


    # print(test.getCurrentCases())