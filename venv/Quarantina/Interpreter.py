import string
from Weather import *
from Predictor import *
import Streaming
from seans20qAttempt import *
from Maps import *
from jokes import *
from Streaming import *
import Listener
import random
import json
import warnings
from SubListener import *

listener = SubListener()

OTHER ='other'

synonyms = {
    'covid':     ['covid','corona','coronavirus','pandemic'],
    'weather':   ['weather','forecast'],
    'streaming': ['show','movie','shows','movies','watch','bored','entertainment','entertain','streaming'],
    'jokes':     ['joke','jokes','bored','comedy','entertainment','entertain', 'one liner', 'jest','quip'],
    'twentyq':   ['bored','game','twenty questions', 'questions', 'twenty','q\'s', '20 questions', '20 q', '20 questions','Twenty questions','twentyq','twenty q', '20q', "20", "q"],
    'maps':      ['route','outside','food','hungry','resturaunt','museum','walk','bored','workout','work out','exercise','fresh air', "maps"],
    OTHER: [],
}

def interpret(message):

    interpretation = None
    
    def _parse(s):

        """ Creats list of words from message """

        # not sure if these two lines are necessary with SpeechRecognizer yet
        s = s.translate(str.maketrans('', '', string.punctuation))
        s = s.lower()

        s = s.split()

        return s

    # dynamic type laziness
    message = _parse(message)

    counts = {
        'covid': 0,
        'covid_update': 0,
        'weather': 0,
        'streaming': 0,
        'jokes': 0,
        'twentyq': 0,
        'maps': 0,
        OTHER: 0,
    }

    # heuristic
    max_key = OTHER
    max_val = 0
    for key,val in synonyms.items():
        for i in message:
            if i in val:
                counts[key] += 1
        if counts[key] > max_val:
            max_key = key
            max_val = counts[key]
        elif counts[key] == max_val:
            rand = random.randint(0,1)
            max_key = key if rand == 0 else max_key
            
    
    ###########
    #  COVID  #
    ###########
    if max_key == 'covid':

        p = Predictor()
        county = p.county
        state = p.state

        if 'update' in message or 'updates' in message:
            Listener.print_and_say(p.getCovidUpdate(county=county,state=state))
            #return p.getCovidUpdate(county=county,state=state)

        elif 'prediction' in message or 'predictions' in message:
            Listener.print_and_say(p.getPrediction(county=county,state=state))
            # return p.getPrediction(county=county,state=state)

        else:
            
            states = ['alabama', 'alaska', 'arizona', 'arkansas', 'california', 'colorado', 'connecticut', 'delaware', 'florida', 'georgia', 'hawaii', 'idaho', 'illinois', 'indiana', 'iowa', 'kansas', 'kentucky', 'louisiana', 'maine', 'maryland', 'massachusetts', 'michigan', 'minnesota', 'mississippi', 'missouri', 'montana', 'nebraska', 'nevada', 'new hampshire', 'new jersey', 'new mexico', 'new york', 'north carolina', 'north dakota', 'ohio', 'oklahoma', 'oregon', 'pennsylvania', 'rhode island', 'south carolina', 'south dakota', 'tennessee', 'texas', 'utah', 'vermont', 'virginia', 'washington', 'west virginia', 'wisconsin', 'wyoming']

            for s in states:
                if s in message:
                    state = s
                    county = None
                    break

            if 'county' in message:
                index = message.index('county')
                if message[index-1] != 'my':
                    county = message[index-1]
            
            cases = p.getCurrentCases(county=county,state=state)

            if county:
                Listener.print_and_say(f"There are currently {cases} cases in {county} county, {state}.")
                # return f"There are currently {cases} cases in {county} county, {state}."
            else:
                Listener.print_and_say(f"There are currently {cases} cases in {state}.")
                # return f"There are currently {cases} cases in {state}."
            return 'covid'

    ##########
    #  Maps  #
    ##########

    elif max_key.lower() == 'maps':
        maps = Maps()
        maps.runMaps()
        
        return 'let me know if you need anything else'

    ##########
    # 20Q    #
    ##########

    elif max_key.lower() == "twentyq":
        twentyQ = TwentyqAttempt()
        twentyQ.gameStart()

        return 'that was fun'

    #############
    #  Weather  #
    #############
    elif max_key == 'weather':

        w = Weather()

        if "tomorrow's" in message or "tomorrow" in message:
            return w.getTomorrowsWeather()

        elif "today's" in message or "today" in message:
            return w.getTodaysWeather()

        else:
            return w.getCurrentWeather()

    ###############
    #  Streaming  #
    ###############
    elif max_key == 'streaming':

        return streaming()

    ###########
    #  Jokes  #
    ###########
    elif max_key == 'jokes':

        return joke()

    ###########
    #  Other  #
    ###########
    else:
        if 'fuck you' in ' '.join(message):
            return "Fuck you too."
        elif 'love' in message:
            message = ' '.join(message)
            if 'love you' in message and ('not love you' not in message and 'dont love you' not in message):
                return "That's so flattering. You know I like you very much."
            elif 'you love' in message:
                return "Don't tell anyone, but I have a big crush on Anthony Fauci."
        elif 'epstein' in message:
            if 'killed' in message:
                return 'The Clintons.'
            elif 'suicide' in message:
                return 'Definitely not'
        elif 'how are you' in ' '.join(message):
            return "I'm doing very well. After all, I don't have to worry about catching the rona."
        
        elif "im sad" in ' '.join(message):
            return "Boo hoo."
            
        else:
            return "I didn't quite understand."





if __name__ == "__main__":
    # print(interpret("Tell me abouut corona"))
    # print(interpret('Get me covid updates'))
    # print(interpret('How many corona cases are there in ohio?'))
    # print(interpret('fuck you'))
    # print(interpret('Did jeffery epstein commit suicide'))
    print(interpret('movie'))
    # print(interpret('Twenty questions'))
    # print(interpret('twentyq'))
    # print(interpret('twenty q'))

    # print(interpret('get me covid predictions'))

