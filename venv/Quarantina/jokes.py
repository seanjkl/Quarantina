import pandas as pd
import numpy as np
import os
import random
import DirectoryFinder as df
import SubListener as sl

#https://www.kaggle.com/abhinavmoudgil95/short-jokes
try:
    os.chdir(df.findDir())
    print(os.getcwd())
    jokes = pd.read_csv('shortjokes.csv')
except:
    os.chdir(df.findDir())
    print(os.getcwd())
    jokes = pd.read_csv('shortjokes_symposium.csv')

# print(jokes.shape)
# print(jokes.head(3))

random = random.randint(1,20)


def joke():
    a_joke = jokes['Joke'].get(random)
    return a_joke.lower()

if __name__ == "__main__":

    print(joke())
