# B351GroupProject
Sean Roach, Ali Alamri, Chris Alexeev

## Quarantina

### Requirements To Run:

Anaconda, including running the project on the Anaconda interpreter. Many of the non native requirements will automatically be installed on your machine by Anaconda.
To run the file, load project (tested in pycharm) and run from the Listener class, because we built this to run on Raspberry pi you will need to run the code in that class, which is what would have been activated
each time the user pressed the button, it essentially is what activates the Listening process(E.G. Hey Quarantina)

##### Package Dependencies:

Try installing with this first

cat requirements_mac.txt | xargs -n 1 pip install

attempts to install them all one by one first, may not succeed.

###### Native in Python 3.7:

- subprocess
- datetime
- warnings
- os
- json
- string
- random
- socket
- requests
- math
- difflib
- sys
- csv



###### Other:

- numpy
- datascience
- matplotlib
- pandas
- pystan ( this can be tricky to install on windows, in order to get this done you may need to do "conda install -m pystan")
- fbprophet
- pyttsx3
- SpeechRecognition
- openrouteservice
- geocoder
- pygeoj
- geopy
- sklearn
- pgeocode
- bs4 (beautiful soup)  



### Troubleshooting

- File Not Found or Directory Not Found issues
  - comment out whereever it says os.chdir(DirectoryFinder.findDir())
    - this bug occurs when the file system for your machine is not a standard mac file system (or you are not running Darwin based mac / posix based mac).\
- KeyError when talking
  - Usually occurs when there is no speech to interpret. Wait to see ("listening") output to the log to begin talking (and then maybe another half second or so). 
- Stuck on Listening
  - Caused by adjusting to (mic.adjustToAmbientNoice), potential fix, is to mute your microphone, or simply wait and it will usually self resolve. However it is still recording during this time.
- Pystan/Prophet Install
  - If you are not running a conda interpreter this will likely not work. Download anaconda (https://www.anaconda.com/).  It requires python 3.7. If pip install does not work, try conda install -m "module".
- PyAudio Install
  - Again, conda install -m "module" works well here, or the package manager in pycharm will work well. Another thing you can try is downloading the gcc compiler version 14.0 from Microsoft as that is likely the dependency issue you are getting.
- Problem with Jokes
  - Make sure the import for jokes matches the same casing as the file, for whatever github did not take care of this when we changed it.
  
### How It All Works Together

The modules all integrate to make one full program. The program runs through the Listener.py, which takes user audio input and sends it to the Interpreter. The Interpreter imports the Predictor, Maps, Twenty Questions, Weather, Streaming, and Jokes modules, and uses the appropriate module for a given input. There are some other auxillary classes such as DataPull and MapAndQListener that are used as utilites for the modules that need them.

### Predictor:
  - Activation Words:
      - 'Covid', 'corona', 'coronavirs', 'pandemic', 'cases', 'data', 'numbers'
      - This module can also be fed special keywords like county or state or update or prediction to get different feed back (e.g. specify state or specify whether you want prediction of 7 days or current county numbers since last update)

  - 7 day US prediction using three different models (linear regression, polynomial fitting, and Facebook Prophet)
    - ```forecastUS_linreg(state=None,county=None)```
    - ```forecastUS_polyfit(state=None,county=None)```
    - ```forecastUS_prophet(state=None,county=None)```
  - 


### Maps

- Activation Words
  - 'route' 'run' 'walk' 'bored' 'workout' 'work out' 'exercise' 'fresh air' "maps"
- This module will use your current location on file (tracked by IP) and find some kind of point of interest near you, and reccomend a walking route.  The functionality for step by step directions is built in however not displayed or provided to the user due to not good way to provide that information.
- POI Filter Criteria can be things like "Restaurant", "Museum", "Parking", "Cafe", "Bicycle", "etc".



### Twenty Questions

- Activation Words
  - 'Game', "games", 'twenty question', 'twenty questions', fun'
- This module takes you through a basic game of twenty questions, directions to play are explained as you go and pretty straightforward.



### Weather ###

- Activation Words
  - "weather", "forecast", "outside"
- This module can also be fed additional input such as "today" or "tomorrow" to get the respective forecast, default it returns current weather.



### Streaming

- Activation Words
  - "streaming", "show", "movie", "shows", "movies", "watch", "bored", "entertainment", "entertain"
- Provide this movie recommender with a movie you would like to see other movie similar too and it will make recommendations of similar movies.



### Jokes

- Activation Words
  - "jokes", "jokes", "laugh", "funny", "giggle", "one liner", "jest" ,"quip" ,"pun"
- this module doesnt take any direct input, it just tells you a joke. 
- NOT RESPONSIBLE FOR HURT FEELINGS WE JUST FOUND A HUGE CSV OF ONE LINERS ONLINE AND SOME ARE NOT PC

## Raspberry Pi

- The Pi version works like the regular version, but needs to be pulled from the pi_branch branch and run using the pi_env virtual environment.
